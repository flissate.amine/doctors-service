package ma.jad.doctors.controller;

import ma.jad.doctors.entities.Doctor;
import ma.jad.doctors.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctors")
public class DoctorController {

    @Autowired
    DoctorRepository doctorRepository;

    @GetMapping("/all")
    @ResponseBody
    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }

    @GetMapping("/doctor/{id}")
    @ResponseBody
    public Doctor getDoctorById(@PathVariable Long idDoctor) {
        return doctorRepository.findById(idDoctor).get();
    }

    @DeleteMapping("/delete")
    public void deleteDoctor(Long idDoctor) {
        doctorRepository.deleteById(idDoctor);
    }

    @PostMapping("/save")
    @ResponseBody
    public void saveDoctor(@PathVariable Doctor doctor) {
        doctorRepository.save(doctor);
    }

}
