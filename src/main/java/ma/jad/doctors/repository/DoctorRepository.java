package ma.jad.doctors.repository;

import ma.jad.doctors.entities.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
	Page<Doctor>findByNameContains(String keyw,Pageable pageable);
}